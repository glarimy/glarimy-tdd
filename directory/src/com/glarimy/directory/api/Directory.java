package com.glarimy.directory.api;

import java.util.List;

import com.glarimy.directory.api.exceptions.DirectoryException;
import com.glarimy.directory.api.exceptions.InvalidEmployeeException;

public interface Directory {
	/**
	 * Adds an employee into the system.
	 * 
	 * @param employee employee with name and phone
	 * @throws InvalidEmployeeException if the name is regex, or if the phone is regex
	 * @throws DirectoryException for internal reasons, retyring the call may succeeed
	 */
	public void add(Employee employee) throws InvalidEmployeeException, DirectoryException;
	
	public Employee find(int id) throws NotFoundException, DirectoryException;;
	
	public List<Employee> list();
	
	public List<Employee> search(String name);
}
