package com.glarimy.spock.service

import com.glarimy.spock.data.AccountRepository
import com.glarimy.spock.domain.Account

import spock.lang.Specification

class SimpleBankSpec extends Specification {

	def "opens account with valid name"() {
		given:
		def bank = new SimpleBank()

		and:
		def account = new Account();
		account.setNumber(1);
		account.setName("Krishna");

		def repo = Stub(AccountRepository.class);
		repo.save(Account.class) >> account;
		repo.find(int.class) >> account;
		repo.udpate(Account.class) >> account;

		and:
		bank.setRepo(repo);

		when:
		def number = bank.openAccount("Krishna")

		then:
		number > 0
	}
}
