package com.glarimy.spock.service;

import com.glarimy.spock.api.Bank;
import com.glarimy.spock.data.AccountRepository;
import com.glarimy.spock.exceptions.AccountNotFoundException;
import com.glarimy.spock.exceptions.BankException;
import com.glarimy.spock.exceptions.InvalidNameException;

public class SimpleBank implements Bank {
	@SuppressWarnings("unused")
	private AccountRepository repo;

	public void setRepo(AccountRepository repo) {
		this.repo = repo;
	}

	@Override
	public int openAccount(String name) throws InvalidNameException, BankException {
		if (name == null || name.trim().length() < 3)
			throw new InvalidNameException();
		return 0;
	}

	@Override
	public double deposit(int number, double amount) throws AccountNotFoundException, BankException {
		return 0;
	}

}
