package com.glarimy.spock.data;

import com.glarimy.spock.domain.Account;

public interface AccountRepository {
	public Account save(Account account);
	public Account find(int number);
	public Account update(Account account);

}
