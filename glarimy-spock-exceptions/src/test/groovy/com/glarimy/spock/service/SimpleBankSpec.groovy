package com.glarimy.spock.service

import com.glarimy.spock.exceptions.AccountNotFoundException
import com.glarimy.spock.exceptions.BankException
import com.glarimy.spock.exceptions.InvalidNameException

import spock.lang.Specification

class SimpleBankSpec extends Specification {
	def bank = new SimpleBank();

	def "opens account with valid name"() {
		when:
		def number = bank.openAccount("Krishna")

		then:
		number > 0
	}

	def "opens account with null name"() {
		when:
		bank.openAccount(null)

		then:
		InvalidNameException ine = thrown();
	}

	def "open account with emty name"() {
		when:
		bank.openAccount("")

		then:
		InvalidNameException ine = thrown();
	}

	def "open account with shorter name"() {
		when:
		bank.openAccount("Kri")

		then:
		InvalidNameException ine = thrown()
	}

	def "deposit amount to non-existing account"() {
		when:
		def number = bank.openAccount("Krishna")
		bank.deposit(number+1, 100);

		then:
		AccountNotFoundException anfe = thrown()
	}

	def "deposit invalid amount"() {
		when:
		def number = bank.openAccount("Krishna")
		bank.deposit(number, 0)

		then:
		BankException anfe = thrown()
	}

	def "deposit valid amount"() {
		when:
		def number = bank.openAccount("Krishna")
		def balance = bank.deposit(number, 100)

		then:
		balance == 100
	}
}
