package com.glarimy.spock.api;

import com.glarimy.spock.exceptions.AccountNotFoundException;
import com.glarimy.spock.exceptions.BankException;
import com.glarimy.spock.exceptions.InvalidNameException;

public interface Bank {
	public int openAccount(String name) throws InvalidNameException, BankException;
	public double deposit(int number, double amount) throws AccountNotFoundException, BankException;
}
