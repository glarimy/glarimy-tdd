package com.glarimy.spock.service;

import java.util.HashMap;
import java.util.Map;

import com.glarimy.spock.api.Bank;
import com.glarimy.spock.domain.Account;
import com.glarimy.spock.exceptions.AccountNotFoundException;
import com.glarimy.spock.exceptions.BankException;
import com.glarimy.spock.exceptions.InvalidNameException;

public class SimpleBank implements Bank {
	private Map<Integer, Account> accounts = new HashMap<Integer, Account>();

	@Override
	public int openAccount(String name) throws InvalidNameException, BankException {
		if (name == null || name.trim().length() < 3)
			throw new InvalidNameException();
		Account account = new Account();
		account.setNumber(accounts.size() + 1);
		account.setName(name);
		accounts.put(account.getNumber(), account);
		return account.getNumber();
	}

	@Override
	public double deposit(int number, double amount) throws AccountNotFoundException, BankException {
		Account account = accounts.get(number);
		if (account == null)
			throw new AccountNotFoundException();
		account.setBalance(account.getBalance() + amount);
		return account.getBalance();
	}

}
