package com.glarimy.tdd;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CalculatorTest {
	@Test
	public void addZeroes() {
		Calculator calc = new Calculator();
		assertEquals("Failed to add two zeroes", 0, calc.add(0, 0));
	}

	@Test
	public void addPositives() {
		Calculator calc = new Calculator();
		assertEquals("Failed to add two positive integers", 15, calc.add(10, 5));
	}

	@Test
	public void addNegatives() {
		Calculator calc = new Calculator();
		assertEquals("Failed to add two negative integers", -15, calc.add(-10, -5));
	}

	@Test
	public void addPositiveAndNegative() {
		Calculator calc = new Calculator();
		assertEquals("Failed to add a positive integer with negative integer", 5, calc.add(10, -5));
	}
}
