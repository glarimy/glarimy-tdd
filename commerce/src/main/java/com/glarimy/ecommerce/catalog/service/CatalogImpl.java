package com.glarimy.ecommerce.catalog.service;

import java.util.List;

import com.glarimy.ecommerce.catalog.api.Catalog;
import com.glarimy.ecommerce.catalog.api.Criteria;
import com.glarimy.ecommerce.catalog.api.Product;
import com.glarimy.ecommerce.catalog.exception.CatalogException;
import com.glarimy.ecommerce.catalog.exception.InvalidCriteriaException;
import com.glarimy.ecommerce.catalog.exception.InvalidProductException;


public class CatalogImpl implements Catalog {

	public int upsert(Product product) throws InvalidProductException, CatalogException {
		if(product.getRating() < 0 || product.getRating() > 5)
			throw new InvalidCriteriaException("Rating must be in range of 1-5");
		return 0;
	}

	public List<Product> search(Criteria criteria) throws InvalidCriteriaException, CatalogException {
		return null;
	}

}
