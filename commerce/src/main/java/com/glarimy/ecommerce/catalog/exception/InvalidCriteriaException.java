package com.glarimy.ecommerce.catalog.exception;

@SuppressWarnings("serial")
public class InvalidCriteriaException extends CatalogException {

	public InvalidCriteriaException() {
		// TODO Auto-generated constructor stub
	}

	public InvalidCriteriaException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InvalidCriteriaException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidCriteriaException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidCriteriaException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
