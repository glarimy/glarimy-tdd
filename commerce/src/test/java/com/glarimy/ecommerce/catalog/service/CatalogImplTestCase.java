package com.glarimy.ecommerce.catalog.service;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.glarimy.ecommerce.catalog.api.Product;
import com.glarimy.ecommerce.catalog.exception.CatalogException;
import com.glarimy.ecommerce.catalog.exception.InvalidProductException;

@RunWith(Parameterized.class)
public class CatalogImplTestCase {
	Product product;

	public CatalogImplTestCase(Product product) {
		this.product = product;
	}

	@Parameters
	public static Collection<Object[]> setup() {
		List<Object[]> input = new ArrayList<Object[]>();
		for (int i = 6; i < 50; i++) {
			Product product = new Product();
			product.setName("Mobile");
			product.setVendor("Samsung");
			product.setPrice(100);
			product.setRating(1);
			input.add(new Object[] { product });
		}
		return input;

	}

	@Test
	public void testUpsertOfProductWithInvalidRating() {
		CatalogImpl catalog = new CatalogImpl();

		try {
			catalog.upsert(this.product);
			fail("inserted product with invalid rating of " + this.product.getRating());
		} catch (InvalidProductException e) {
		} catch (CatalogException e) {
			fail("failed to throw invalid exception");
		}
	}
}
