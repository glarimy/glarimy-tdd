package com.glarimy.junit.service;

public class Account {
	private String customerName;
	private long number;
	private double balance;

	public Account() {

	}

	public Account(String customerName, int number, double balance) {
		super();
		this.customerName = customerName;
		this.number = number;
		this.balance = balance;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public long getNumber() {
		return number;
	}

	public void setNumber(long number) {
		this.number = number;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "Account [customerName=" + customerName + ", number=" + number
				+ ", balance=" + balance + "]";
	}

}
