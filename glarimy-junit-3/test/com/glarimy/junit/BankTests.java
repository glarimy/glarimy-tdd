package com.glarimy.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.glarimy.junit.db.DAOTest;
import com.glarimy.junit.service.SimpleBankTest;

@RunWith(Suite.class)
@SuiteClasses({ SimpleBankTest.class, DAOTest.class })
public class BankTests {

}
