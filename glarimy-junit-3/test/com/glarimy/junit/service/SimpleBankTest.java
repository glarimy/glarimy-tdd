package com.glarimy.junit.service;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;

import com.glarimy.junit.api.BankException;
import com.glarimy.junit.db.DAO;

public class SimpleBankTest {
	SimpleBank bank;
	DAO dao;

	@Before
	public void setUp() throws Exception {
		DAO dao = mock(DAO.class);

		Account validAccount = new Account();
		validAccount.setCustomerName("Krishna Koyya");
		validAccount.setNumber(22061971);
		validAccount.setBalance(3000);

		doNothing().when(dao).saveOrUpdate(any(Account.class));
		doThrow(BankException.class).when(dao).fetch(22071972);
		doReturn(validAccount).when(dao).fetch(22061971);

		bank = new SimpleBank();
		bank.setDao(dao);
	}

	@Test
	public void testOpenAccount() {
		try {
			bank.openAccount("Krishna Koyya");
		} catch (Exception e) {
			fail("Failed to open account!");
		}
	}

	@Test
	public void testOpenAccountWithFirstOrLastName() {
		try {
			bank.openAccount("Krishna");
			fail("Opened account with only first/last name");
		} catch (Exception e) {
		}

	}

	@Test
	public void testOpenAccountWithEmptyName() {
		try {
			bank.openAccount("");
			fail("Opened account with empty name");
		} catch (Exception e) {
		}

	}

	@Test
	public void testOpenAccountWithNullName() {
		try {
			bank.openAccount(null);
			fail("Opened account with null name");
		} catch (Exception e) {
		}

	}

	@Test
	public void testOpenAccountWithShortName() {
		try {
			bank.openAccount("K Koyya");
			fail("Opened account with shorter name");
		} catch (Exception e) {
		}

	}

	@Test
	public void testOpenAccountWithLengthyName() {
		try {
			bank.openAccount("KrishnaMohanKoyyaOfTadepalligudem Glarimy");
			fail("Opened account with lengthy name");
		} catch (Exception e) {
		}

	}

	@Test
	public void testOpenAccountWithoutDatabase() {
		try {
			bank.setDao(null);
			bank.openAccount("Krishna Koyya");
			fail("Opened account without database");
		} catch (Exception e) {
		}

	}

	@Test
	public void testDeposite() {
		try {
			double balance = bank.deposit(22061971, 1000);
			assertTrue("Balance is wrong", balance == 4000);
		} catch (Exception e) {
			fail("Failed to deposite");
		}
	}

	@Test
	public void testDepositeIntoNonExistingAccount() {
		try {
			bank.deposit(22061972, 1000);
			fail("Depositing into non-existing account");
		} catch (Exception e) {
		}
	}

	@Test
	public void testLessDeposite() {
		try {
			bank.deposit(22061971, 100);
			fail("Deposited too less");
		} catch (Exception e) {
		}

	}

	@Test
	public void testLotOFDeposite() {
		try {
			bank.deposit(22061971, 1000000);
			fail("Deposited too much");
		} catch (Exception e) {
		}

	}

	@Test
	public void testWithdraw() {
		try {
			double balance = bank.withdraw(22061971, 1000);
			assertTrue("Balance is wrong", balance == 2000);
		} catch (Exception e) {
			fail("Failed to withdraw");
		}

	}

	@Test
	public void testWithdrawIntoNonExistingAccount() {
		try {
			bank.deposit(22061972, 1000);
		} catch (Exception e) {
			fail("Withdrawing into non-existing account");
		}

	}

	@Test
	public void testToomuchWithdraw() {
		try {
			bank.withdraw(22061971, 100000);
			fail("Withdrawn more than what it have");
		} catch (BankException e) {
		}

	}

}