package com.glarimy.patterns;

public class LoggingProxy implements Component {
	private Component target;

	public LoggingProxy(Component target) {
		this.target = target;
	}

	@Override
	public void handle() {
		System.out.println("LoggingProxy::handle - entering");
		target.handle();
		System.out.println("LoggingProxy::handle - leaving");
	}
}