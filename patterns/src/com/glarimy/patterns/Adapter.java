package com.glarimy.patterns;

public interface Adapter {
	public void perform();
}
