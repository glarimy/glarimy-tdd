package com.glarimy.patterns;

public class ConcreteComponent implements Component {

	@Override
	public void handle() {
		System.out.println("ConcreteComponent::handle");
		
	}

}
