package com.glarimy.patterns;

public class ComponentAdapter implements Adapter {
	private Component component;

	public ComponentAdapter() throws Exception {
		component = (Component) Factory.getInstance("component");
	}

	@Override
	public void perform() {
		System.out.println("ComponentAdapter::perform::pre-processing");
		component.handle();
		System.out.println("ComponentAdapter::perform::post-processing");
	}

}
