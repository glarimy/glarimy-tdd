package com.glarimy.patterns;

import java.io.FileReader;
import java.util.Properties;

public class Factory {
	public static Object getInstance(String key) throws Exception {
		Properties props = new Properties();
		props.load(new FileReader("config.properties"));
		Class claz = Class.forName(props.getProperty(key));
		Object o = claz.newInstance();
		String logger = props.getProperty(key + ".logger");
		if (logger == null)
			return o;
		Class proxy = Class.forName(logger);
		return proxy.getConstructor(proxy.getInterfaces()[0]).newInstance(o);
	}
}
