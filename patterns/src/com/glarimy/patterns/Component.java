package com.glarimy.patterns;

public interface Component {
	public void handle();
}
