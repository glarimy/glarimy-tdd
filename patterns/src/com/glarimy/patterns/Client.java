package com.glarimy.patterns;

public class Client {
	public static void main(String[] args) throws Exception {
		Adapter adapter = (Adapter)Factory.getInstance("adapter");
		adapter.perform();
	}
}
