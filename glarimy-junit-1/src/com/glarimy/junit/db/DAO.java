package com.glarimy.junit.db;

import java.util.HashMap;
import java.util.Map;

import com.glarimy.junit.api.BankException;
import com.glarimy.junit.service.Account;

public class DAO {
	Map<Long, Account> ledger = new HashMap<>();

	public Account fetch(long number) throws BankException {
		if (ledger.containsKey(number))
			return ledger.get(number);
		throw new BankException("Account Not Found");
	}

	public void saveOrUpdate(Account account) throws BankException {
		ledger.put(account.getNumber(), account);
	}
}
