package com.glarimy.junit.api;

public interface Bank {
	/**
	 * Opens any number of accounts for a valid customer name with first and
	 * last names. The name must have a minimum length of 8 and a maximum length
	 * of 32 characters. Only English alphabetical characters are allowed. One
	 * space between first and last name is allowed.
	 * 
	 * @param customerName
	 *            a valid customer name
	 * @return number of the newly created account
	 * @throws BankException
	 *             if the customer name is invalid or for any other internal
	 *             reasons
	 */
	public long openAccount(String customerName) throws BankException;

	/**
	 * Deposits specified amount in to an account with specified number.
	 * 
	 * @param number
	 *            valid and existing account number
	 * @param amount
	 *            valid amount of minimum Rs. 500 and maximum of Rs. 500000
	 * @return latest balance of the account after deposit
	 * @throws BankException
	 *             if input is invalid or for internal reasons
	 */
	public double deposit(long number, double amount) throws BankException;

	/**
	 * Withdraws specified amount from an account with specified number.
	 * 
	 * @param number
	 *            valid and existing account number
	 * @param amount
	 *            up to a maximum of current balance in the account
	 * @return latest balanced of the account after withdrawal
	 * @throws BankException
	 *             if the input is invalid or because of insufficient balance or
	 *             for internal reasons
	 */
	public double withdraw(long number, double amount) throws BankException;

	/**
	 * Gets the account balance
	 * 
	 * @param number
	 *            valid and existing account number
	 * @return latest balance
	 * @throws BankException
	 */
	public double getBalance(long number) throws BankException;
}
