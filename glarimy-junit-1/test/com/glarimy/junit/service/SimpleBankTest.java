package com.glarimy.junit.service;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.glarimy.junit.api.BankException;
import com.glarimy.junit.db.DAO;

public class SimpleBankTest {
	SimpleBank bank;
	DAO dao;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		bank = new SimpleBank();
		dao = new DAO();
		bank.setDao(dao);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testOpenAccount() {
		try {
			bank.openAccount("Krishna Koyya");
		} catch (Exception e) {
			fail("Failed to open account!");
		}
	}

	@Test
	public void testOpenAccountWithFirstOrLastName() {
		try {
			bank.openAccount("Krishna");
			fail("Opened account with only first/last name");
		} catch (Exception e) {
		}

	}

	@Test
	public void testOpenAccountWithEmptyName() {
		try {
			bank.openAccount("");
			fail("Opened account with empty name");
		} catch (Exception e) {
		}

	}

	@Test
	public void testOpenAccountWithNullName() {
		try {
			bank.openAccount(null);
			fail("Opened account with null name");
		} catch (Exception e) {
		}

	}

	@Test
	public void testOpenAccountWithShortName() {
		try {
			bank.openAccount("K Koyya");
			fail("Opened account with shorter name");
		} catch (Exception e) {
		}

	}

	@Test
	public void testOpenAccountWithLengthyName() {
		try {
			bank.openAccount("KrishnaMohanKoyyaOfTadepalligudem Glarimy");
			fail("Opened account with lengthy name");
		} catch (Exception e) {
		}

	}

	@Test
	public void testOpenAccountWithoutDatabase() {
		try {
			bank.setDao(null);
			bank.openAccount("Krishna Koyya");
			fail("Opened account without database");
		} catch (Exception e) {
		}

	}

	@Test
	public void testDeposite() {
		long number = 0;
		try {
			number = bank.openAccount("Krishna Koyya");
		} catch (Exception e) {
			fail("failed to open account");
		}
		try {
			double balance = bank.deposit(number, 1000);
			assertTrue("Balance is wrong", balance == 1000);
			balance = bank.deposit(number, 1400);
			assertTrue("Balance is wrong", balance == 2400);
		} catch (Exception e) {
			fail("Failed to deposite");
		}

	}

	@Test
	public void testLessDeposite() {
		long number = 0;
		try {
			number = bank.openAccount("Krishna Koyya");
		} catch (Exception e) {
			fail("failed to open account");
		}
		try {
			bank.deposit(number, 100);
			fail("Deposited too less");
		} catch (Exception e) {
		}

	}

	@Test
	public void testLotOFDeposite() {
		long number = 0;
		try {
			number = bank.openAccount("Krishna Koyya");
		} catch (Exception e) {
			fail("failed to open account");
		}
		try {
			bank.deposit(number, 1000000);
			fail("Deposited too much");
		} catch (Exception e) {
		}

	}

	@Test
	public void testWithdraw() {
		long number = 0;
		double balance = 0;
		try {
			number = bank.openAccount("Krishna Koyya");
		} catch (Exception e) {
			fail("failed to open account");
		}
		try {
			balance = bank.deposit(number, 1000);
			assertTrue("Balance is wrong", balance == 1000);
		} catch (Exception e) {
			fail("Wrong deposite");
		}
		try {
			balance = bank.withdraw(number, 100);
			assertTrue("Balance is wrong", balance == 900);
		} catch (Exception e) {
			fail("Failed to withdraw");
		}

	}

	@Test
	public void testToomuchWithdraw() {
		long number = 0;
		double balance = 0;
		try {
			number = bank.openAccount("Krishna Koyya");
		} catch (Exception e) {
			fail("failed to open account");
		}
		try {
			balance = bank.deposit(number, 1000);
			assertTrue("Balance is wrong", balance == 1000);
		} catch (Exception e) {
			fail("Wrong deposite");
		}
		try {
			balance = bank.withdraw(number, 100000);
			fail("Withdrawn more than what it have");
		} catch (BankException e) {
			fail("Failed to withdraw");
		}

	}

}
