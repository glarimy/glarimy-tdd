package com.glarimy.junit.db;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.glarimy.junit.api.BankException;
import com.glarimy.junit.service.Account;

public class DAOTest {
	DAO dao;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		dao = new DAO();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFetchAtStart() {
		try {
			dao.fetch(12345);
			fail("Fetching data from empty dataset");
		} catch (BankException be) {

		}
	}

	@Test
	public void testFetchWithData() {
		Account account = new Account();
		account.setNumber(1234);
		account.setCustomerName("Krishna");
		account.setBalance(500);

		try {
			dao.saveOrUpdate(account);
		} catch (BankException be) {
			fail("Failed to save an account");
		}

		try {
			Account result = dao.fetch(account.getNumber());
			assertEquals("Fetched account is not the expected",
					account.getNumber(), result.getNumber());
			assertEquals("Fetched account balance is wrong",
					account.getBalance(), result.getBalance(), 0);
			assertEquals("Fetched account holder name is wrong",
					account.getCustomerName(), result.getCustomerName());
		} catch (BankException be) {
			fail("Failed to fetch an existing account");
		}
	}

	@Test
	public void testUpdateData() {
		Account account = new Account();
		account.setNumber(1234);
		account.setCustomerName("Krishna");
		account.setBalance(500);

		try {
			dao.saveOrUpdate(account);
		} catch (BankException be) {
			fail("Failed to save an account");
		}

		try {
			Account result = dao.fetch(account.getNumber());
			assertEquals("Fetched account is not the expected",
					account.getNumber(), result.getNumber());
			assertEquals("Fetched account balance is wrong",
					account.getBalance(), result.getBalance(), 0);
			assertEquals("Fetched account holder name is wrong",
					account.getCustomerName(), result.getCustomerName());
		} catch (BankException be) {
			fail("Failed to fetch an existing account");
		}

		try {
			account.setBalance(800);
			dao.saveOrUpdate(account);
		} catch (BankException be) {
			fail("Failed to update an account");
		}

		try {
			Account result = dao.fetch(account.getNumber());
			assertEquals("Fetched account is not the expected",
					account.getNumber(), result.getNumber());
			assertEquals("Fetched account balance is wrong",
					account.getBalance(), result.getBalance(), 0);
			assertEquals("Fetched account holder name is wrong",
					account.getCustomerName(), result.getCustomerName());
		} catch (BankException be) {
			fail("Failed to fetch an existing account");
		}

	}

	@Test
	public void testUpdateInvalidData() {
		Account account = new Account();
		account.setNumber(1234);
		account.setCustomerName("Krishna");
		account.setBalance(500);

		try {
			dao.saveOrUpdate(account);
		} catch (BankException be) {
			fail("Failed to save an account");
		}

		try {
			Account result = dao.fetch(account.getNumber());
			assertEquals("Fetched account is not the expected",
					account.getNumber(), result.getNumber());
			assertEquals("Fetched account balance is wrong",
					account.getBalance(), result.getBalance(), 0);
			assertEquals("Fetched account holder name is wrong",
					account.getCustomerName(), result.getCustomerName());
		} catch (BankException be) {
			fail("Failed to fetch an existing account");
		}

		try {
			account.setNumber(-1);
			dao.saveOrUpdate(account);
			fail("Updating/saving account with invalid number");
		} catch (BankException be) {
		}

	}

	@Test
	public void testSaveInvalidData() {
		try {
			dao.saveOrUpdate(null);
			fail("Saving a null data");
		} catch (BankException be) {

		}

	}

	@Test
	public void testFetchWithoutData() {
		Account account = new Account();
		account.setNumber(1234);
		account.setCustomerName("Krishna");
		account.setBalance(500);

		try {
			dao.saveOrUpdate(account);
		} catch (BankException be) {
			fail("Failed to save an account");
		}

		try {
			dao.fetch(account.getNumber() + 1);
			fail("Fetching a non-existing data");
		} catch (BankException be) {
			fail("Failed to fetch an existing account");
		}
	}

}