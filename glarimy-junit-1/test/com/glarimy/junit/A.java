package com.glarimy.junit;

public class A {
	IB obj;
	
	public A(IB b) {
		this.obj = b;
	}
	
	public void func() {
		int val = obj.m1(2);
		// uses val
	}

}
