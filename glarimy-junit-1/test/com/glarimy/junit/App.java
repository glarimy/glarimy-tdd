package com.glarimy.junit;

public class App {
	public static void main(String[] args) {
		IB b = new B();
		IB o = new IBMock();
		A a = new A(o);
		a.func();
	}
}
