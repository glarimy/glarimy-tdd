package com.glarimy.junit.service;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.experimental.theories.DataPoint;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import com.glarimy.junit.db.DAO;

@RunWith(Theories.class)
public class SimpleBankTest {
	public static class Data {
		Account account;
		double amount;
		double balance;

		public Data(Account account, double amount, double balance) {
			super();
			this.account = account;
			this.amount = amount;
			this.balance = balance;
		}
	}

	@DataPoint
	public static Data data1 = new Data(new Account("Krishna", 123, 1000), 500,
			1500);
	@DataPoint
	public static Data data2 = new Data(new Account("Krishna", 123, 2000), 500,
			2500);
	@DataPoint
	public static Data data3 = new Data(new Account("Krishna", 123, 0), 500,
			500);
	@DataPoint
	public static Data data4 = new Data(new Account("Krishna", 123, 3000), 200,
			3200);
	@DataPoint
	public static Data data5 = new Data(new Account("Krishna", 123, 4000), 400,
			4400);

	@Theory
	public void testDeposit(Data d) {
		SimpleBank bank = new SimpleBank();
		DAO dao = mock(DAO.class);

		try {
			when(dao.fetch(d.account.getNumber())).thenReturn(d.account);
			bank.setDao(dao);
			double balance = bank.deposit(d.account.getNumber(), d.amount);
			assertTrue("Wrong balance after deposit", balance == d.balance);
		} catch (Exception e) {
			fail("Failed to deposit");
		}

	}

	@Test
	public void testWithdraw() {
		fail("Not yet implemented");
	}

	@Test
	public void testOpenAccount() {
		fail("Not yet implemented");
	}

}
