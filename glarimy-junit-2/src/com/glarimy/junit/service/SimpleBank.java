package com.glarimy.junit.service;

import java.util.Date;

import com.glarimy.junit.api.Bank;
import com.glarimy.junit.api.BankException;
import com.glarimy.junit.db.DAO;

public class SimpleBank implements Bank {
	private DAO dao;

	public DAO getDao() {
		return dao;
	}

	public void setDao(DAO dao) {
		this.dao = dao;
	}

	@Override
	public long openAccount(String customerName) throws BankException {
		if (null == customerName || 0 == customerName.trim().length())
			throw new BankException("Invalid Input");
		Account account = new Account();
		account.setNumber(new Date().getTime());
		account.setCustomerName(customerName);
		account.setBalance(0);
		dao.saveOrUpdate(account);
		return account.getNumber();
	}

	@Override
	public double deposit(long number, double amount) throws BankException {
		Account account = dao.fetch(number);
		account.setBalance(account.getBalance() + amount);
		dao.saveOrUpdate(account);
		return account.getBalance();
	}

	@Override
	public double withdraw(long number, double amount) throws BankException {
		Account account = dao.fetch(number);
		if (account.getBalance() < amount)
			throw new BankException();
		account.setBalance(account.getBalance() + amount);
		dao.saveOrUpdate(account);
		return account.getBalance();
	}

	@Override
	public double getBalance(long number) throws BankException {
		return 0;
	}
}