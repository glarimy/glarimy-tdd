package com.glarimy.junit.db;

import static com.glarimy.junit.IsSameAs.isSameAs;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import com.glarimy.junit.api.BankException;
import com.glarimy.junit.service.Account;

public class DAOTest {
	DAO dao;

	@Before
	public void setUp() throws Exception {
		dao = new DAO();
	}

	@Test
	public void testFetchAtStart() {
		try {
			dao.fetch(12345);
			fail("Fetching data from empty dataset");
		} catch (BankException be) {

		}
	}

	@Test
	public void testFetchWithData() {
		Account account = new Account();
		account.setNumber(1234);
		account.setCustomerName("Krishna");
		account.setBalance(500);

		try {
			dao.saveOrUpdate(account);
		} catch (BankException be) {
			fail("Failed to save an account");
		}

		try {
			Account result = dao.fetch(1234);
			assertThat(result, isSameAs(account));
		} catch (BankException be) {
			fail("Failed to fetch an existing account");
		}
	}

	@Test
	public void testUpdateData() {
		Account account = new Account();
		account.setNumber(1234);
		account.setCustomerName("Krishna");
		account.setBalance(500);

		try {
			dao.saveOrUpdate(account);
		} catch (BankException be) {
			fail("Failed to save an account");
		}

		try {
			Account result = dao.fetch(account.getNumber());
			assertThat(result, isSameAs(account));
		} catch (BankException be) {
			fail("Failed to fetch an existing account");
		}

		try {
			account.setBalance(800);
			dao.saveOrUpdate(account);
		} catch (BankException be) {
			fail("Failed to update an account");
		}

		try {
			Account result = dao.fetch(account.getNumber());
			assertThat(result, isSameAs(account));
		} catch (BankException be) {
			fail("Failed to fetch an existing account");
		}

	}

	@Test
	public void testUpdateInvalidData() {
		Account account = new Account();
		account.setNumber(1234);
		account.setCustomerName("Krishna");
		account.setBalance(500);

		try {
			dao.saveOrUpdate(account);
		} catch (BankException be) {
			fail("Failed to save an account");
		}

		try {
			Account result = dao.fetch(account.getNumber());
			assertThat(result, isSameAs(account));
		} catch (BankException be) {
			fail("Failed to fetch an existing account");
		}

		try {
			account.setNumber(-1);
			dao.saveOrUpdate(account);
			fail("Updating/saving account with invalid number");
		} catch (BankException be) {
		}

	}

	@Test
	public void testSaveInvalidData() {
		try {
			dao.saveOrUpdate(null);
			fail("Saving a null data");
		} catch (BankException be) {

		}

	}

	@Test
	public void testFetchWithoutData() {
		Account account = new Account();
		account.setNumber(1234);
		account.setCustomerName("Krishna");
		account.setBalance(500);

		try {
			dao.saveOrUpdate(account);
		} catch (BankException be) {
			fail("Failed to save an account");
		}

		try {
			dao.fetch(account.getNumber() + 1);
			fail("Fetching a non-existing data");
		} catch (BankException be) {
			fail("Failed to fetch an existing account");
		}
	}

}