package com.glarimy.junit;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;

import com.glarimy.junit.service.Account;

public class IsSameAs extends BaseMatcher<Account> {
	private Account expected;

	public IsSameAs(Account expected) {
		this.expected = expected;
	}

	@Override
	public void describeTo(Description msg) {
		msg.appendText("" + expected);
	}

	@Override
	public boolean matches(Object actual) {
		if (actual instanceof Account) {
			Account account = (Account) actual;
			if (expected.getNumber() != account.getNumber()) {
				return false;
			}
			if (expected.getBalance() != account.getBalance()) {
				return false;
			}
			if (!expected.getCustomerName().equals(account.getCustomerName())) {
				return false;
			}
			return true;
		}
		return false;
	}

	@Factory
	public static <T> Matcher<Account> isSameAs(Account expected) {
		return new IsSameAs(expected);
	}
}
