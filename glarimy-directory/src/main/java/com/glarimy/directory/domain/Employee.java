package com.glarimy.directory.domain;

public class Employee {
	public int eid;
	public String name;
	public long phone;
	public String email;

	public Employee(String name, long phone, String email){
		this.name = name;
		this.phone = phone;
		this.email = email;
	}
}
