package com.glarimy.directory.service;

public interface Cache {
	public void put(Object k, Object v) throws RuntimeException;
	public Object get(Object k) throws RuntimeException;
}
