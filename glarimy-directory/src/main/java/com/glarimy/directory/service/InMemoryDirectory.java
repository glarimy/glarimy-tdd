package com.glarimy.directory.service;

import com.glarimy.directory.domain.Employee;
import com.glarimy.directory.api.*;

public class InMemoryDirectory implements  Directory {
	private Cache cache;

	public InMemoryDirectory(Cache cache) {
		this.cache = cache;
	}	
	public int add(Employee employee) throws InvalidEmployeeException, DuplicateEmployeeException, DirectoryException {
		if(employee == null)
			throw new InvalidEmployeeException();
		if(employee.name == null || employee.name.trim().length() == 0)
			throw new InvalidEmployeeException();
		if(employee.phone < 1)
			throw new InvalidEmployeeException();
		return 0;
	}
	public Employee find(int eid) throws EmployeeNotFoundException, DirectoryException {
		return null;
	}
}
