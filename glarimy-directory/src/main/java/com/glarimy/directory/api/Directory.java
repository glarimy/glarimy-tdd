package com.glarimy.directory.api;

import com.glarimy.directory.domain.Employee;

public interface Directory {
	public int add(Employee employee) throws InvalidEmployeeException, DuplicateEmployeeException, DirectoryException;
	public Employee find(int eid) throws EmployeeNotFoundException, DirectoryException;
}
