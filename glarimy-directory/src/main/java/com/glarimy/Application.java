package com.glarimy.directory;

import com.glarimy.directory.api.*;
import com.glarimy.directory.domain.*;
import com.glarimy.directory.service.*;

public class Application {
	public static void main(String[] args) throws Exception {
		Cache cache = new DistributedCache();
		Directory dir = new InMemoryDirectory(cache);
		System.out.println(dir.add(null));
		System.out.println(dir.find(1));
	}
}
