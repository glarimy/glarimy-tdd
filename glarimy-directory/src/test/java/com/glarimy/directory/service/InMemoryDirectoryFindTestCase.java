package com.glarimy.directory.service;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.glarimy.directory.api.*;
import com.glarimy.directory.domain.*;
import com.glarimy.directory.service.*;

import static org.mockito.Mockito.*;

@RunWith(Parameterized.class)
public class InMemoryDirectoryFindTestCase {
	Employee employee;
	int id;

	public InMemoryDirectoryFindTestCase(Employee employee, int id) {
		this.employee = employee;
		this.id = id;
	}

	@Parameters
	public static Collection<Object[]> setup() {
		List<Object[]> input = new ArrayList<>();
		input.add(new Object[] {new Employee("Krishna", 123, "abc@xyz.com"), 1});
		
		return input;
	}

	@Test
	public void testAddEmployee() {
		Cache cache = mock(Cache.class);
		doNothing().when(cache).put(any(Integer.class), any(Employee.class));
		doReturn(this.employee).when(cache).get(this.id);
		Directory dir = new InMemoryDirectory(cache);

		try {
			dir.add(this.employee);
			Employee result = dir.find(this.id);
			assertTrue(result != null);
			assertTrue(result.eid == this.employee.eid);
			assertTrue(result.phone == this.employee.phone);
		} catch (DirectoryException e) {
			fail("faile to find employees");
		}
	}
}

