package com.glarimy.directory.service;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.glarimy.directory.api.*;
import com.glarimy.directory.domain.*;
import com.glarimy.directory.service.*;

import static org.mockito.Mockito.*;

@RunWith(Parameterized.class)
public class InMemoryDirectoryAddTestCase {
	Employee employee;

	public InMemoryDirectoryAddTestCase(Employee employee) {
		this.employee = employee;
	}

	@Parameters
	public static Collection<Object[]> setup() {
		List<Object[]> input = new ArrayList<>();
		input.add(new Object[] {new Employee("", 123, "abc@xyz.com")});
		input.add(new Object[] {new Employee("Krishna", -123, "abc@xyz.com")});
		input.add(new Object[] {new Employee("Krishna", 123, "abcxyz.com")});
		
		return input;
	}

	@Test
	public void testAddEmployee() {
		Cache cache = mock(Cache.class);
		doNothing().when(cache).put(any(Integer.class), any(Employee.class));
		Directory dir = new InMemoryDirectory(cache);

		try {
			dir.add(this.employee);
			fail("adding ivnalid employees");
		} catch (InvalidEmployeeException e) {
		} catch (DirectoryException e) {
			fail("throwing worn exceptions");
		}
	}
}

