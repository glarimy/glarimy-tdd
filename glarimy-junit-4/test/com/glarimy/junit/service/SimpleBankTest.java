package com.glarimy.junit.service;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.glarimy.junit.db.DAO;

@RunWith(Parameterized.class)
public class SimpleBankTest {
	private Account account;
	private double amount;
	private double dbalance;
	private double wbalance;

	public SimpleBankTest(Account account, double amount,
			double db, double wb) {
		this.account = account;
		this.amount = amount;
		this.dbalance = db;
		this.wbalance = wb;
	}

	@Parameters
	public static Collection<Object[]> init() {
		ArrayList<Object[]> data = new ArrayList<>();
		data.add(new Object[] { new Account("Krishna", 123, 1000), 500, 1500, 500 });
		data.add(new Object[] { new Account("Krishna", 123, 2000), 0, 2000 });
		data.add(new Object[] { new Account("Krishna", 123, 0), 1000, 1000 });
		data.add(new Object[] { new Account("Krishna", 123, 3000), 200, 3200 });
		data.add(new Object[] { new Account("Krishna", 123, 4000), 400, 4400 });
		return data;
	}

	@Test
	public void testDeposit() {
		SimpleBank bank = new SimpleBank();
		//DAO dao = mock(DAO.class);

		try {
			//when(dao.fetch(this.account.getNumber())).thenReturn(this.account);
			//bank.setDao(dao);
			double balance = bank
					.deposit(this.account.getNumber(), this.amount);
			assertTrue("Wrong balance after deposit", balance == this.dbalance);
		} catch (Exception e) {
			fail("Failed to deposit");
		}

	}

	@Test
	public void testWithdraw() {
		fail("Not yet implemented");
	}

	@Test
	public void testOpenAccount() {
		fail("Not yet implemented");
	}

}
