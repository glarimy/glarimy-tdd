package com.glarimy.ecommerce.catalog.exception;

@SuppressWarnings("serial")
public class CatalogException extends Exception {

	public CatalogException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CatalogException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CatalogException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CatalogException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CatalogException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}
