package com.glarimy.ecommerce.catalog.api;

import java.util.List;

import com.glarimy.ecommerce.catalog.exception.CatalogException;
import com.glarimy.ecommerce.catalog.exception.InvalidCriteriaException;
import com.glarimy.ecommerce.catalog.exception.InvalidProductException;

/**
 * Interface for catalog service. 
 * 
 * @author glarimy
 *
 */

public interface Catalog {
	/**
	 * Adds a new product or updates an existing product item in the catalog.
	 * 
	 * If the productID is a non-zero positive value, the operation is considered as
	 * update. If the productID is zero, the operation is considered as insert. If
	 * the productID is a negative value, operation will be rejected.
	 * 
	 * @param product the details of the product.
	 * @return id of the newly added product
	 * @throws InvalidProductException if id is negative number or if name or vendor
	 *                                 is missing or price/popularity is not a
	 *                                 positive number. Also, if no product is found
	 *                                 with the specified ID, if present.
	 * @throws CatalogException        for any internal reasons.
	 */
	public int upsert(Product product) throws InvalidProductException, CatalogException;

	/**
	 * Searches and returns zero or more products, based on the supplied criteria.
	 * Criteria includes parameters: product name, vendor name, price range and
	 * popularity. Either product name or vendor name is mandatory. If more than one
	 * parameter is specified, products that meat any one or more of the parameters
	 * will be returned.
	 * 
	 * @param criteria search criteria.
	 * @return A list of size zero or more, with products
	 * @throws InvalidCreteriaException if neither the name of the product nor the
	 *                                  name of the vendor is not specified, or max
	 *                                  price is less than min price.
	 * @throws CatalogException         for any internal reasons.
	 */
	public List<Product> search(Criteria criteria) throws InvalidCriteriaException, CatalogException;

}
