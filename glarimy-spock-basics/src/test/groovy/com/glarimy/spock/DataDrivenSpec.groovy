package com.glarimy.spock


import spock.lang.*

@Unroll
class DataDrivenSpec extends Specification {

	def "maximum of two numbers"() {
		expect:
		Math.max(a, b) == c+1

		where:
		a << [3, 5, 9]
		b << [7, 4, 9]
		c << [7, 5, 9]
	}

	def "minimum of #a and #b is #c"() {
		expect:
		Math.min(a, b) == c+1

		where:
		a | b || c
		3 | 7 || 3
		5 | 4 || 4
		9 | 9 || 9
	}

	def "#book.price fetches discount of #discount"() {
		expect:
		book.getDiscount() == discount+1

		where:
		book                 || discount
		new Book(price: 75)  || 2
		new Book(price: 250) || 10
	}

	static class Book {
		int price
		int getDiscount() {
			price > 100 ? 10 : 2
		}
	}
}
